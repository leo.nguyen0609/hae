CREATE TABLE USER (
	user_id INT auto_increment,
	username VARCHAR ( 100 ) NOT NULL,
	password VARCHAR ( 255 ) NULL,
	role INT NOT NULL,
	updated_at INT NULL,
	created_at INT NULL,
PRIMARY KEY ( user_id ) 
)