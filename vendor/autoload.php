<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/9/2019
 * Time: 6:45 PM
 */

  if(file_exists('../vendor/env.php')) {
      include '../vendor/env.php';
  }
  if(!function_exists('env')) {
      function env($key, $default = null)
      {
          $value = getenv($key);
          if ($value === false) {
              return $default;
          }
          return $value;
      }
  }
