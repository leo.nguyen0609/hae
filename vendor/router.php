<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/3/2019
 * Time: 11:39 AM
 */


class Router
{
    public $group = [];

    public $routes = [];

    public function __construct()
    {
        //apc_clear_cache();
    }

    public function addRouter($method, $uri, $action)
    {
        $uri = rtrim($uri, '/');
        if (empty($uri)) {
            $uri = $_SERVER['SERVER_NAME'];
        }
        $this->routes[$uri]['namespace'] = $this->group['namespace'] ?? null;
        $this->routes[$uri]['method'] = $method;
        $this->routes[$uri]['action'] = $action;
        $this->routes[$uri]['uri'] = $uri;
        apcu_store($uri, $this->routes[$uri]);
        unset($this->routes);
        unset($this->group);
    }

    public function get($uri, $route)
    {
        if (is_array($route)) {
            if (!empty($this->group['prefix'])) {
                $uri = sprintf('/%s/%s', trim($this->group['prefix'], '/'), trim($uri, '/'));
            } else {
                $uri = sprintf('/%s', trim($uri, '/'));
            }
        }

        $action = $route['action'];

        return $this->addRouter('GET', $uri, $action);
    }

    public function post($uri, array $route)
    {
        if (is_array($route)) {
            if (!empty($this->group['prefix'])) {
                $uri = sprintf('/%s/%s', trim($this->group['prefix'], '/'), trim($uri, '/'));
            } else {
                $uri = sprintf('/%s', trim($uri, '/'));
            }
        }
        $action = $route['action'];

        return $this->addRouter('POST', $uri, $action);
    }

    public function put($uri, array $route)
    {
        if (is_array($route)) {
            if (!empty($this->group['prefix'])) {
                $uri = sprintf('/%s/%s', trim($this->group['prefix'], '/'), trim($uri, '/'));
            } else {
                $uri = sprintf('/%s', trim($uri, '/'));
            }
        }
        $action = $route['action'];

        return $this->addRouter('PUT', $uri, $action);
    }

    public function delete($uri, array $route)
    {
        if (is_array($route)) {
            if (!empty($this->group['prefix'])) {
                $uri = sprintf('/%s/%s', trim($this->group['prefix'], '/'), trim($uri, '/'));
            } else {
                $uri = sprintf('/%s', trim($uri, '/'));
            }
        }
        $action = $route['action'];

        return $this->addRouter('DELETE', $uri, $action);
    }

    public function group(array $group, Closure $router)
    {
        if (!empty($group['namespace'])) {
            $this->group['namespace'] = $group['namespace'] ?? null;
        }
        if (!empty($group['prefix'])) {
            $this->group['prefix'] = $group['prefix'] ?? null;
        }

        return $router($this);
    }

}