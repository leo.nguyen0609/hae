<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/3/2019
 * Time: 10:44 AM
 */

require_once __DIR__ . '/router.php';

require '../app/guestbook/controllers/GuestbookController.php';
require '../app/api/controllers/GuestbookController.php';
require '../app/IndexController.php';


class Application
{
    public $router;

    public function __construct()
    {
        $this->router = new Router();
    }

    public function run()
    {
        $route = $this->getCurrentRoute();
        if (empty($route)) {
            return http_response_code(404);
        }
        $method = $route['method'];
        if ($this->validMethod($method) > 0) {
            return http_response_code(405);
        }
        $action = explode('@', $route['action']);
        $class = sprintf('%s\%s', $route['namespace'], $action[0]);
        $func = sprintf('%s', $action[1]);
        $object = new $class();
        $object->$func();
        
    }

    private function getCurrentRoute()
    {
        $current_uri = rtrim(strtok($_SERVER["REQUEST_URI"],'?'), '/');
        if (empty($current_uri)) {
            $current_uri = $_SERVER['SERVER_NAME'];
            $route = apcu_fetch($current_uri);
        } else {
            $route = apcu_fetch($current_uri);
        }

        return $route;
    }

    private function validMethod($method)
    {
        if ($_SERVER['REQUEST_METHOD'] == $method) {
            return 0;
        }

        return 1;
    }

}