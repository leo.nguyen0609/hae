function submit() {
    username = $("input[name='username']").val();
    message = $("textarea[name='message']").val();
    data = {
        'username': username,
        'message': message
    };
    $.post("/api/post", data, function (response, status) {
        location.reload();
        // html = getHtml(response);
        // $('.right').append(html)
    });
}

function getHtml(data) {
    var obj = JSON.parse(data);
    html =
        '<div class="col-sm-6 col-12 item">' +
        obj.message +
        '<div class="row">' +
        '<div class="col-sm-6 col-6">' +
        '<span class="name">' + obj.username + '</span>' +
        '<span class="time">' + new Date(obj.created_at * 1000).toDateString() + '</span>' +
        '</div>' +
        '<div class="col-sm-6 col-6">' +
        '<i class="fas fa-trash icon" alt="Avatar"></i>' +
        '<i class="fas fa-pencil-alt icon" alt="Avatar"></i>' +
        '</div>' +
        '</div>' +
        '</div>';
    return html;
}