<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/31/2019
 * Time: 12:51 PM
 */
require_once __DIR__ . '/../vendor/application.php';

require_once __DIR__ . '/../vendor/define.php';

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Application();

$app->router->group(['namespace' => 'app\guestbook\controllers'], function ($router) {
    require __DIR__ . '/../routes/web.php';
});
$app->router->group(['namespace' => 'app\api\controllers'], function ($router) {
    require __DIR__ . '/../routes/api.php';
});
$app->router->group(['namespace' => 'app'], function ($router) {
    require __DIR__ . '/../routes/routes.php';
});
$app->run();
