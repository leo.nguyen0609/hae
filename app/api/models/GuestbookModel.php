<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/9/2019
 * Time: 2:52 PM
 */

namespace app\api\models;


use app\objects\GuestbookObject;

include '../app/api/models/Model.php';
include '../app/objects/GuestbookObject.php';

class GuestbookModel extends Model
{
    public static function create(array $data)
    {
        $guestObject = new GuestbookObject($data);

        return $guestObject->insert();
    }

    public static function find(int $id)
    {
        $guestObject = new GuestbookObject();

        return $guestObject->find($id);
    }

    public static function getList(int $page, int $limit)
    {
        $guestObject = new GuestbookObject();

        $data ['data'] = $guestObject->getList($page, $limit);
        $totalPage = $guestObject->getTotalPage($limit);
        $data['paginator'] = ['current' => $page, 'limit' => $limit, 'total' => $totalPage];

        return $data;
    }


}