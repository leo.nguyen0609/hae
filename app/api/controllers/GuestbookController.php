<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/4/2019
 * Time: 12:33 PM
 */

namespace app\api\controllers;


use app\api\models;
use app\api\models\GuestbookModel;

include 'Controller.php';
include '../app/api/models/GuestbookModel.php';

class GuestbookController extends Controller
{
    public function get()
    {
    }

    public function getList()
    {

    }

    public function post()
    {
        $data = $this->getParams();
        $guestbook_id = GuestbookModel::create($data);
        $guestbook = GuestbookModel::find($guestbook_id);
        echo json_encode($guestbook);
    }

    private function getParams()
    {
        return [
            'message'  => $_POST['message'] ?? null,
            'username' => $_POST['username'] ?? null
        ];
    }

}