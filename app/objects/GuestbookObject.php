<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/9/2019
 * Time: 2:53 PM
 */

namespace app\objects;

include '../app/objects/BaseObject.php';

class GuestbookObject extends BaseObject
{
    protected $_tableName = 'guestbook';

    protected $_primaryKey = 'guestbook_id';

    protected $_fields = [
        'message',
        'username',
        'created_at',
        'updated_at'
    ];

    public function insert()
    {
        $sql = "insert into guestbook (message, username, updated_at, created_at) values(:message, :username, :updated_at, :created_at)";
        $this->_query = $sql;

        return $this->save();
    }

    public function find(int $id)
    {
        $sql = "select * from $this->_tableName where $this->_primaryKey = ?";
        $this->_query = $sql;

        return $this->getByKey($id);
    }

    public function getList(int $page, $limit = 10)
    {
        $offset = ($page - 1) * $limit;
        $sql = "select * from $this->_tableName order by created_at desc limit $offset, $limit";
        $this->_query = $sql;
        return $this->select();
    }

    public function getTotalPage($limit){
        $this->getTotal();

        return ceil($this->_total/$limit);
    }



}