<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/9/2019
 * Time: 2:53 PM
 */

namespace app\objects;

use PDO;

include '../app/objects/Connection.php';

class BaseObject extends \stdClass
{
    protected $_tableName;

    protected $_primaryKey;

    protected $_fields;

    protected $_data;

    protected $_created_at;

    protected $_updated_at;

    protected $_pdo;

    protected $_query;

    protected $_total;

    public function __construct(array $data = null)
    {
        if (!empty($data)) {
            $this->setAttribute($data);
        }
        Connection::connect();
        $this->_pdo = Connection::$con;
    }

    private function setAttribute(array $data = null)
    {
        foreach ($this->_fields as $key => $value) {
            if (array_key_exists($value, $data)) {
                $this->_data[$value] = $data[$value];
            }
        }
        $this->_data['created_at'] = $this->_created_at;
        $this->_data['updated_at'] = $this->_updated_at;
    }

    public function save()
    {
        try {
            $this->_data['updated_at'] = strtotime('now');
            $this->_data['created_at'] = strtotime('now');
            $this->_pdo->prepare($this->_query)->execute($this->_data);

            return $this->_pdo->lastInsertId();
        } catch (\PDOException $e) {
            throw $e;
        }
    }

    public function getByKey(int $id)
    {
        try {
            $query = $this->_pdo->prepare($this->_query);
            $query->execute([$id]);

            return $query->fetch(PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            throw $e;
        }

    }

    public function select()
    {
        try {
            //var_dump($this->_query);die;
            $query = $this->_pdo->prepare($this->_query);
            $query->execute();

            return $query->fetchAll(PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            throw $e;
        }
    }

    public function getTotal()
    {
        $sql = "select count(*) from $this->_tableName";
        $count = $this->_pdo->query($sql)->fetchColumn();
        $this->_total = $count;
    }


}