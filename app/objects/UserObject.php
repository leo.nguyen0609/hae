<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/9/2019
 * Time: 3:20 PM
 */

namespace app\objects;


class UserObject extends BaseObject
{
    protected $_tableName = 'user';

    protected $_primaryKey = 'user_id';

    protected $_fields = [
        'user_id',
        'username',
        'role',
        'created_at',
        'updated_at'
    ];
}