<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/9/2019
 * Time: 3:07 PM
 */

namespace app\objects;

use PDO;

class Connection
{
    public static $con;

    public static function connect()
    {
        $servername = env('DB_HOST');
        $dbname = env('DB_DATABASE');
        $port = env('DB_PORT');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        try {
            $conn = new PDO("mysql:host=$servername;port=$port;dbname=$dbname", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$con = $conn;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}