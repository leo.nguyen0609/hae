<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/3/2019
 * Time: 2:38 PM
 */

namespace app\guestbook\controllers;

use app\api\models\GuestbookModel;
use app\guestbook\helper\view;


include 'Controller.php';
include '../app/guestbook/helper/View.php';

class GuestbookController extends Controller
{

    public function index()
    {
        !isset($_GET['page']) ? $page = 1 : $page = $_GET['page'];
        $list = GuestbookModel::getList($page, $size = 10);
        $list['paginator'] = $this->paginator($list['paginator']);
        return (new View('index', $list));
    }

    public function get()
    {
    }

    public function getList()
    {

    }

    public function post()
    {

    }

    private function getParams()
    {
        $data['message'] = $_POST['message'] ?? null;
        $data['username'] = $_POST['username'] ?? null;

        return $data;
    }

    public function paginator(array $data)
    {
        $str = null;
        $countPage = $data['total'];
        $current = $data['current'];
        for ($i = 1; $i <= $countPage; $i++) {
            $style = $current == $i ? 'style="background-color: #C0213E"' : '';
            $str .= sprintf('<a href="?page=%d" %s>%d</a>', $i, $style, $i);
        }

        return $str;
    }

}