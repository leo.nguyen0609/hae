<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/7/2019
 * Time: 8:33 PM
 */

namespace app\guestbook\helper;


class View
{
    public function __construct($view, array $data = null)
    {
        $str = $this->readView($view, true, $data);
        $layout = file_get_contents('../resources/view/layout.html');
        $view = str_replace("@content", $str, $layout);
        echo $view;
    }

    private function readView($view, $store = false, array $data = null)
    {
        if (!empty($data)) {
            extract($data);
        }

        ob_start();
        include(VIEW_PATH . '/' . $view . '.php');
        if ($store) {
            return ob_get_clean();
        } else {
            ob_end_flush();
        }
    }

}