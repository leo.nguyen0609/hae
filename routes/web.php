<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 3/31/2019
 * Time: 12:51 PM
 */
//$router->get('/', [
//    'action' => 'GuestbookController@index'
//]);
$router->group(['prefix' => 'guestbook'], function ($router) {
    $router->get('/', [
        'action' => 'GuestbookController@index'
    ]);
    $router->get('/get-list', [
        'action' => 'GuestbookController@getList',
    ]);
    $router->get('/get', [
        'action' => 'GuestbookController@get',
    ]);
    $router->post('/post', [
        'action' => 'GuestbookController@post',
    ]);
});






