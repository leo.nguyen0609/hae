<?php
/**
 * Created by PhpStorm.
 * User: congn
 * Date: 4/4/2019
 * Time: 12:27 PM
 */

$router->group(['prefix' => 'api'], function ($router) {
    $router->post('/post', [
        'action' => 'GuestbookController@post',
    ]);
});
