<div class="row">
    <?php include '../resources/view/left.php' ?>
    <div class="col-sm-9">
        <div class="row right">
            <?php if (!empty($data)) { ?>
                <?php foreach ($data as $item) { ?>
                    <div class="col-sm-6 col-12 item">
                        <?php echo $item->message ?? null ?>
                        <div class="row">
                            <div class="col-sm-6 col-6">
                                <span class="name"><?php echo $item->username ?? null ?></span>
                                <span class="time"><?php echo date('D M d Y', $item->created_at) ?? null ?></span>
                            </div>
                            <div class="col-sm-6 col-6">
                                <i class="fas fa-trash icon" alt="Avatar"></i>
                                <i class="fas fa-pencil-alt icon" alt="Avatar"></i>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-sm-12 col-12 wrapper-pagination">
                <div class="pagination">
                    <?php echo $paginator??null ?>
                </div>
            </div>
        </div>
    </div>
</div>